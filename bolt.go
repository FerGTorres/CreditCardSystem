package main

import (
	"encoding/json"
	"fmt"
	bolt "github.com/coreos/bbolt"
	"log"
	"os"
    "time"
	"os/exec"
    "strconv"
)

type cliente struct {
	Nrocliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

type tarjeta struct {
	Nrotarjeta   string
	Nrocliente   int
	Validadesde  string
	Validahasta  string
	Codseguridad string
	Limitecompra float64
	Estado       string
}

type comercio struct {
	Nrocomercio  int
	Nombre       string
	Domicilio    string
	Codigopostal string
	Telefono     string
}

type compra struct {
	Nrooperacion int
	Nrotarjeta   string
	Nrocomercio  int
	Fecha        time.Time
	Monto        float64
	Pagado       bool
}

func leer(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte
	// abre una transaccion de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})
	return buf, err
}

func agregarInstacia(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

func crearYCargarBaseDeDatos() {
	db, err := bolt.Open("dbBolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	//CLIENTE 1
	instancia := cliente{0, "Jose", "Garcia", "Mitre 548", "011-57601415"}
	data, err := json.Marshal(instancia)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "cliente", []byte(strconv.Itoa(instancia.Nrocliente)), data)
	resultado, err := leer(db, "cliente", []byte(strconv.Itoa(instancia.Nrocliente)))
	fmt.Printf("%s\n", resultado)
	//CLIENTE 2
	instancia = cliente{1, "Maria Carmen", "Martinez", "Sarmiento 6852", "011-64851679"}
	data, err = json.Marshal(instancia)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "cliente", []byte(strconv.Itoa(instancia.Nrocliente)), data)
	resultado, err = leer(db, "cliente", []byte(strconv.Itoa(instancia.Nrocliente)))
	fmt.Printf("%s\n", resultado)
	//CLIENTE 3
	instancia = cliente{2, "Francisco", "Lopez", "Peron 66622", "011-63425186"}
	data, err = json.Marshal(instancia)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "cliente", []byte(strconv.Itoa(instancia.Nrocliente)), data)
	resultado, err = leer(db, "cliente", []byte(strconv.Itoa(instancia.Nrocliente)))
	fmt.Printf("%s\n", resultado)
	//TARJETA 1
	instancia2 := tarjeta{"5553279369819851", 0, "202003", "202512", "4046", 30000.00, "vigente"}
	data, err = json.Marshal(instancia2)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "tarjeta", []byte(instancia2.Nrotarjeta), data)
	resultado, err = leer(db, "tarjeta", []byte(instancia2.Nrotarjeta))
	fmt.Printf("%s\n", resultado)
	//TARJETA 2
	instancia2 = tarjeta{"5596862656844653", 1, "202003", "202511", "3295", 380000.00, "vigente"}
	data, err = json.Marshal(instancia2)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "tarjeta", []byte(instancia2.Nrotarjeta), data)
	resultado, err = leer(db, "tarjeta", []byte(instancia2.Nrotarjeta))
	fmt.Printf("%s\n", resultado)
	//TARJETA 3
	instancia2 = tarjeta{"5197781489519196", 2, "202004", "202508", "3258", 40000.00, "vigente"}
	data, err = json.Marshal(instancia2)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "tarjeta", []byte(instancia2.Nrotarjeta), data)
	resultado, err = leer(db, "tarjeta", []byte(instancia2.Nrotarjeta))
	fmt.Printf("%s\n", resultado)
	//COMERCIO 1
	instancia3 := comercio{0, "Garbarino", "Hipólito Yrigoyen 1778", "B1665ABR", "011-47999451"}
	data, err = json.Marshal(instancia3)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "comercio", []byte(strconv.Itoa(instancia3.Nrocomercio)), data)
	resultado, err = leer(db, "comercio", []byte(strconv.Itoa(instancia3.Nrocomercio)))
	fmt.Printf("%s\n", resultado)
	//COMERCIO 2
	instancia3 = comercio{1, "McDonals", "Arturo Umberto Illia 3770", "B1613HRS", "011-99965424"}
	data, err = json.Marshal(instancia3)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "comercio", []byte(strconv.Itoa(instancia3.Nrocomercio)), data)
	resultado, err = leer(db, "comercio", []byte(strconv.Itoa(instancia3.Nrocomercio)))
	fmt.Printf("%s\n", resultado)
	//COMERCIO 3
	instancia3 = comercio{2, "CompumundoHiperMegaRed", "Honduras 4865", "C1414BMM", "011-19483767"}
	data, err = json.Marshal(instancia3)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "comercio", []byte(strconv.Itoa(instancia3.Nrocomercio)), data)
	resultado, err = leer(db, "comercio", []byte(strconv.Itoa(instancia3.Nrocomercio)))
	fmt.Printf("%s\n", resultado)
	//COMPRA 1
	//nrooperacion,nrotarjeta,nrocomercio,fecha,monto,pagado
	instancia4 := compra{0, "5553279369813969", 0, time.Now(), 3567.25, false}
	data, err = json.Marshal(instancia4)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "compra", []byte(strconv.Itoa(instancia4.Nrooperacion)), data)
	resultado, err = leer(db, "compra", []byte(strconv.Itoa(instancia4.Nrooperacion)))
	fmt.Printf("%s\n", resultado)
	//COMPRA 2
	instancia4 = compra{1, "5596862656844653", 1, time.Now(), 1400.00, false}
	data, err = json.Marshal(instancia4)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "compra", []byte(strconv.Itoa(instancia4.Nrooperacion)), data)
	resultado, err = leer(db, "compra", []byte(strconv.Itoa(instancia4.Nrooperacion)))
	fmt.Printf("%s\n", resultado)
	//COMPRA 3
	instancia4 = compra{2, "5197781489519196", 2, time.Now(), 12500.00, false}
	data, err = json.Marshal(instancia4)
	if err != nil {
		log.Fatal(err)
	}
	agregarInstacia(db, "compra", []byte(strconv.Itoa(instancia4.Nrooperacion)), data)
	resultado, err = leer(db, "compra", []byte(strconv.Itoa(instancia4.Nrooperacion)))
	fmt.Printf("%s\n", resultado)
}

func dataEntered() string {
	var teclado string
	fmt.Printf("(boltDB)\n")
	fmt.Printf("\n\t\t\t\t\t\tBIENVENIDO\n\n")
	fmt.Printf("Opciones: \n")
	fmt.Printf("\t\t1): Crear base de datos y cargar datos.\n")
	fmt.Printf("\t\t2): Salir.\n")
	fmt.Printf("Ingrese la opcion deseada: ")
	fmt.Scanf("%s", &teclado)
	return teclado
}

func limpiarPantalla() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
}

func main() {
	limpiarPantalla()
	run := true
	for run {
		teclado := dataEntered()
		limpiarPantalla()
		switch teclado {
		case "1":
			crearYCargarBaseDeDatos()
		case "2":
			run = false
		default:
			fmt.Printf("¡Opcion inválida!\n")
			fmt.Printf("¡Ingrese número del 1 al 2!")
		}
		var basura string
		if run {
			fmt.Scanf("%v", &basura)
		}
		limpiarPantalla()
	}
}
