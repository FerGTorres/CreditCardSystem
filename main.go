package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"os/exec"
)

//------------------------------ METODOS DE LA INTERFAZ ----------------------------------------------------------------

func correMenu() string {
	var teclado string
	fmt.Printf("\n\t\t\t\t\t\tBIENVENIDO\n\n")
	fmt.Printf("Opciones: \n")
	fmt.Printf("\t\t1): Crear base de datos.\n")
	fmt.Printf("\t\t2): Crear tablas.\n")
	fmt.Printf("\t\t3): Agregar PK y FK.\n")
	fmt.Printf("\t\t4): Cargar los datos.\n")
	fmt.Printf("\t\t5): Cargar stored procedures y triggers.\n")
	fmt.Printf("\t\t6): Generar consumos\n")
	fmt.Printf("\t\t7): Generar resumenes\n")
	fmt.Printf("\t\t8): Eliminar PK  y FK.\n")
	fmt.Printf("\t\t9): Salir.\n")
	fmt.Printf("Ingrese la opcion deseada: ")
	fmt.Scanf("%s", &teclado)
	return teclado
}

func main() {
	limpiarPantalla() //limpia pantalla
	run := true
	//Banderas para el manejo de restricciones y avisos
	dbCreada := false
	tablasCreadas := false
	datoscreados := false
	pkfkCreadas := false
	spAndTriggersCreados := false
	consumoscreados := false
	resumencreado := false
	//ciclo del programa
	for run {
		teclado := correMenu()
		limpiarPantalla()
		//switch para capturar la opcion que el usuario desea
		switch teclado {
		case "1":
			if !dbCreada {
				crearDatabase()
				dbCreada = true
			} else if dbCreada {
				fmt.Print("Base de datos creada\n")
			}
		case "2":
			if dbCreada && !tablasCreadas {
				crearTablas()
				tablasCreadas = true
			} else if tablasCreadas {
				fmt.Print("Tablas creadas\n")
			} else {
				fmt.Print("Primero debe crear la base de datos\n")
			}
		case "3":
			if tablasCreadas && !pkfkCreadas {
				cargarPKs()
				cargarFKs()
				pkfkCreadas = true
			} else if pkfkCreadas {
				fmt.Print("Primary keys y Foreign Keys, creadas\n")
			} else {
				fmt.Print("Primero se debe cargar las tablas\n")
			}
		case "4":
			if tablasCreadas && !datoscreados {
				cargarCliente()
				cargarComercio()
				cargarConsumo()
				cargarCierre()
				cargarTarjeta()
				datoscreados = true
			} else if datoscreados {
				fmt.Print("Datos cargados en la base de datos(clientes, comercios, consumos, cierres y tarjetas)\n")
			} else {
				fmt.Print("Primero debe crear las Pks y FKs\n")
			}

		case "5":
			if dbCreada && tablasCreadas && datoscreados && pkfkCreadas && !spAndTriggersCreados {
				crearStoredProcedure()
				spAndTriggersCreados = true
			} else if spAndTriggersCreados {
				fmt.Print("Stored procedures y triggers cargados en la base de datos\n")
			} else {
				fmt.Print("Primero debe crear la base de datos, sus tablas, clientes, comercios, foreign keys y primary keys\n")
			}

		case "6":
			if dbCreada && tablasCreadas && datoscreados && pkfkCreadas && spAndTriggersCreados && !consumoscreados {
				autorizar_compra()
				consumoscreados = true
			} else if consumoscreados {
				fmt.Print("Consumos cargados\n")
			} else {
				fmt.Print("Primero debe crear la base de datos, sus tablas, clientes, comercios, PK,FK Stored procedure y Trigger\n")
			}
		case "7":
			if dbCreada && tablasCreadas && datoscreados && pkfkCreadas && spAndTriggersCreados && consumoscreados && !resumencreado {
				generarResumen()
				resumencreado = true
			} else if resumencreado {
				fmt.Print("Resumenes cargados\n")
			} else {
				fmt.Print("Primero debe crear la base de datos, sus tablas, clientes, comercios, PK,FK Stored procedure, Trigger y generar compras\n")
			}

		case "8":
			if dbCreada && tablasCreadas && datoscreados && pkfkCreadas {
				eliminarFks()
				eliminarPks()
				pkfkCreadas = false
			} else if !pkfkCreadas {
				fmt.Print("Las PKs y FKs no existian\n")
			} else {
				fmt.Print("Para eliminarlas tienen que estar creadas!\n")
			}
		case "9":
			run = false
		default:
			fmt.Printf("¡Opcion inválida!\n")
			fmt.Printf("¡Ingrese número del 1 al 9!")
		}
	}
}

func limpiarPantalla() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
}


//------------------------------ METODOS QUE GENERAN LA BD Y SUS INSTANCIAS ----------------------------------------------------------------

func crearDatabase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable") // conexion a psql
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	fmt.Printf("Conectado con Postgres\n")

	_, err = db.Exec(`drop database if exists bddtp`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create database bddtp`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Base de datos creada\n")
}

func crearTablas() {

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, erro := db.Exec(`create table cliente(nrocliente int, nombre text,apellido text, domicilio text, telefono char(12));
	
	                    create table tarjeta(nrotarjeta char(16), nrocliente int, validadesde char(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10));
	                    
						create table comercio(nrocomercio int, nombre text, domicilio text, codigopostal char(8), telefono char(12));
	                    
						create table compra(nrooperacion int, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean);
	                    
						create table rechazo(nrorechazo int, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text);
						
						create table cierre(anio int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date);
	                    
						create table cabecera(nroresumen int, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(8,2));
	                    
						create table detalle(nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2));
	                    
						create table alerta(nroalerta int, nrotarjeta char(16), fecha timestamp, nrorechazo int, codalerta int, descripcion text);
	                    
						create table consumo(nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2));`)
	if erro != nil {
		log.Fatal(err)
	}
	fmt.Printf("Tablas creadas\n")
}

func cargarPKs() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable") // conexion a psql
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, erro := db.Exec(`alter table cliente add constraint nrocliente_pk primary key (nrocliente);
                        alter table tarjeta add constraint nrotarjeta_pk primary key (nrotarjeta);
                        alter table comercio add constraint nrocomercio_pk primary key (nrocomercio);
                        alter table compra add constraint nrooperacion_pk primary key (nrooperacion);
                        alter table rechazo add constraint nrorechazo_pk primary key (nrorechazo);
                        alter table cierre add constraint anio_mes_terminacion_pk primary key (anio,mes,terminacion);
                        alter table cabecera add constraint nroresumen_pk primary key (nroresumen);
                        alter table detalle add constraint nroresumen_nrolinea_pk primary key (nroresumen,nrolinea);
                        alter table alerta add constraint nroalerta_pk primary key (nroalerta);`)
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Printf("Pks cargadas\n")
}

func cargarFKs() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, erro := db.Exec(`alter table tarjeta add constraint FKtarjetanrocliente foreign key (nrocliente) references cliente (nrocliente);
                        alter table cabecera add constraint FKcabeceranrotarjeta  foreign key (nrotarjeta) references tarjeta (nrotarjeta);
                        alter table compra add constraint FKcompranrocomercio foreign key (nrocomercio) references comercio (nrocomercio);
						alter table compra add constraint FKcompranrotarjeta foreign key (nrotarjeta) references tarjeta (nrotarjeta);
						--alter table rechazo add constraint FKrechazonrotarjeta foreign key (nrotarjeta) references tarjeta (nrotarjeta);
						alter table rechazo add constraint FKrechazonrocomercio foreign Key (nrocomercio) references comercio (nrocomercio);
						--alter table alerta add constraint FKalertanrotarjeta foreign key (nrotarjeta) references tarjeta (nrotarjeta);
                        alter table alerta add constraint FKalertanrorechazo foreign key (nrorechazo) references rechazo (nrorechazo);`)
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Printf("FKs cargadas\n")
}

func eliminarPks() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable") //conexion a psql
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	// ejecutar los comandos para eliminar las pk
	_, erro := db.Exec(`alter table alerta drop constraint nroalerta_pk;
                        alter table detalle drop constraint nroresumen_nrolinea_pk;
                        alter table cabecera drop constraint nroresumen_pk;
                        alter table cierre drop constraint anio_mes_terminacion_pk;
                        alter table rechazo drop constraint nrorechazo_pk;
                        alter table compra drop constraint nrooperacion_pk;
                        alter table comercio drop constraint nrocomercio_pk;
                        alter table tarjeta drop constraint nrotarjeta_pk;
                        alter table cliente drop constraint nrocliente_pk;`)
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Printf("Pks eliminadas\n")
}

func eliminarFks() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, erro := db.Exec(`alter table tarjeta drop constraint FKtarjetanrocliente;
						
						alter table cabecera drop constraint FKcabeceranrotarjeta;
						
                        alter table compra drop constraint  FKcompranrocomercio;
						alter table compra drop constraint FKcompranrotarjeta;
						
						--alter table rechazo drop constraint FKrechazonrotarjeta;
						alter table rechazo drop constraint FKrechazonrocomercio;

                        alter table alerta drop constraint  FKalertanrorechazo;
						--alter table alerta drop constraint  FKalertanrotarjeta
						`)

	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Printf("FKs eliminadas\n")
}

func cargarCliente() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, er := db.Exec(`insert into cliente values( 0, 'Julio', 'Iglesias','Brasil 6143','011-59684528');
                      insert into cliente values( 1, 'Patricia', 'Sosa','San Martin 7812','011-59684586');
	                  insert into cliente values( 2, 'Adrian', 'Ciro','San Juan 3497','011-19657943');
	                  insert into cliente values( 3, 'Antonio','Rios','Uruguay 1576','011-61947744');
	                  insert into cliente values( 4, 'Leonel','Mattioli','Sarmiento 2655','011-93587525');
	                  insert into cliente values( 5, 'Rodrigo', 'Bueno','Chaco 1677','011-16782595');
	                  insert into cliente values( 6, 'Pablo','Ortega','Cuba 9435','011-36587591');
	                  insert into cliente values( 7, 'Camilo','Cesto','Belgrano 7158','011-74889578');
	                  insert into cliente values( 8, 'Ricardo','Arjona','Jujuy 1149','011-85969586');
	                  insert into cliente values( 9, 'Eros','Ramazotti','España 3685','011-15482588');
	                  insert into cliente values( 10, 'Joaquin','Sabina','Rosas 9785','011-66956979');
	                  insert into cliente values( 11, 'Diego','Torres','Ushuaia 1144','011-19667825');
	                  insert into cliente values( 12, 'Luis','Miguel','Colombia 6677','011-77589266');
	                  insert into cliente values( 13, 'Enrique','Morales','Mitre 2288','011-78914578');
	                  insert into cliente values( 14, 'Jose Luis','Morales','Chile 9173','011-62784545');
	                  insert into cliente values( 15, 'Alberto','Napolitano','Formosa 4496','011-81927155');
	                  insert into cliente values( 16, 'Julieta','Venegas', 'Artigas 7732','011-81942673');
	                  insert into cliente values( 17, 'Luciano','Pereyra','Bolivar 1668','011-92716282');
	                  insert into cliente values( 18, 'Gustavo','Cerati','Roma 3478','011-18729582');
	                  insert into cliente values( 19, 'Pablo','Lescano','Corrientes 5578','011-63415241');`)
	if er != nil {
		log.Fatal(er)
	}
	fmt.Printf("Clientes cargados\n")
}

func cargarConsumo() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable") // conexion a postgres
	if err != nil {

		log.Fatal(err)

	}
	defer db.Close()
	_, erro := db.Exec(`insert into consumo values('5800279369813969','8541',09,1000);

                        insert into consumo values('5800279369813969','8541',05,1000);

                        insert into consumo values('6674862656844653','5576',05,2000);

                        insert into consumo values('3210781489519196','3357',02,5000);
 
                        insert into consumo values('0025215961815467','9317',03,2000);

                        insert into consumo values('5201749684282950','7031',11,2000);

                        insert into consumo values('6374012766885347','9901',02,1000.00);

                        insert into consumo values('6374012766885347','9901',02,1900.00);

                        insert into consumo values('6374012766885347','9901',02,200.00);

                        insert into consumo values('3674656314167645','6321',01,2000);

                        insert into consumo values('3674656314167645','6321',16,2000);

                        insert into consumo values('3566111384747251','4857',19,2000);

                        insert into consumo values('3566111384747251','4857',20,2000);

                        insert into consumo values('9741112487023475','8137',20,2000);

                        insert into consumo values('5911718181747681','9632',10,2000);

                        insert into consumo values('6688134012375322','3573',12,500);

                        insert into consumo values('1122007172229162','9519',07,2000);

                        insert into consumo values('1122007172229162','9518',06,2000);

                        insert into consumo values('6574547618543472','8376',04,2000);

                        insert into consumo values('9585760797853770','6584',20,30000);

                        insert into consumo values('6547338853663039','5104',17,20000);

                        insert into consumo values('2040329005482057','4579',14,2000);

                        insert into consumo values('2040329005482057','4579',12,2000);

                        insert into consumo values('4513564012375322','4263',12,2000);

                        insert into consumo values('4024007172229162','5104',09,20000);

                        insert into consumo values('4024007172229162','5104',01,2000);

                        insert into consumo values('1010833689558208','1025',10,2000);

                        `)
	if erro != nil {

		log.Fatal(err)

	}
	fmt.Printf("Consumos cargados\n")
}

func cargarComercio() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, bug := db.Exec(`insert into comercio values( 01, 'Coto',            'Peron 5678',   'B1665ABR', '011-47999451');
	                    insert into comercio values( 02, 'Terrazas de Mayo','Dinamarca 65', 'B1613HRS', '011-99965424');
	                    insert into comercio values( 03, 'Dionysos',        'Cordoba 150',  'C1414BMM', '011-19483767');
	                    insert into comercio values( 04, 'Kevingston',      'Savedra 951',  'B1744OHX', '011-32488412');
	                    insert into comercio values( 05, 'Dexter',          'Rusia 789',    'B1686FCF', '011-68716455');
	                    insert into comercio values( 06, 'Dash Depotes',    'Formosa 654',  'B1619JHP', '011-68524455');
	                    insert into comercio values( 07, 'Victor Hogar',    'Moreno 861',   'B1635MSC', '011-21574785');
	                    insert into comercio values( 08, 'Ocn',             'Camerun 7417', 'B1748FXC', '011-17554236');
	                    insert into comercio values( 09, 'Tropea',          'Belgrano 453', 'B6700APJ', '011-67948122');
	                    insert into comercio values( 10, 'Fravega',         'Canada 7851',  'B1615KUT', '011-69874125');
	                    insert into comercio values( 11, 'Garbarino',       'Mendoza 300',  'B1646CXG', '011-64552287');
	                    insert into comercio values( 12, 'Easy',            'Rosas 10',     'B1712HIS', '011-33448765');
	                    insert into comercio values( 13, 'Nike',            'Grecia 1508',  'B1640FRE', '011-24356699');
	                    insert into comercio values( 14, 'Adidas Store',    'Misiones 657', 'B1722ERG', '011-67551148');
	                    insert into comercio values( 15, 'Samsung',         'Güemes 897',   'B1873BOC', '011-66448501');
	                    insert into comercio values( 16, 'Musimundo',       'Noruega 7045', 'B1665ABR', '011-25843367');
	                    insert into comercio values( 17, 'Trevi',           'Alvear 2790',  'B1621EYB', '011-34587211');
	                    insert into comercio values( 18, 'Cinemark',        'Japon 8008',   'C1184AAN', '011-55996731');
	                    insert into comercio values( 19, 'Mostaza',         'Salta 979',    'B1714GDM', '011-66771249');
	                    insert into comercio values( 20, 'McDonald´s',      'Rivadavia 163','B1706AYJ', '011-15748521');`)
	if bug != nil {
		log.Fatal(bug)
	}
	fmt.Printf("Comercios cargados\n")
}

func cargarCierre() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	} //la apertura de la tarjeta es el primer día del mes y se cuentan 10 días de cierre + 3 de vencimiento. No lo estiro mas por los meses que tienen menos días...
	defer db.Close()
	_, er := db.Exec(`insert into cierre values(2020,01,1,'2020-01-01','2020-01-11','2020-01-14'); 
                      insert into cierre values(2020,01,2,'2020-01-02','2020-01-12','2020-01-15');
                      insert into cierre values(2020,01,3,'2020-01-03','2020-01-13','2020-01-16');
                      insert into cierre values(2020,01,4,'2020-01-04','2020-01-14','2020-01-17');
                      insert into cierre values(2020,01,5,'2020-01-05','2020-01-15','2020-01-18');
                      insert into cierre values(2020,01,6,'2020-01-06','2020-01-16','2020-01-19');
                      insert into cierre values(2020,01,7,'2020-01-07','2020-01-17','2020-01-20');
                      insert into cierre values(2020,01,8,'2020-01-08','2020-01-18','2020-01-21');
                      insert into cierre values(2020,01,9,'2020-01-09','2020-01-19','2020-01-22');
                      insert into cierre values(2020,01,0,'2020-01-10','2020-01-20','2020-01-23');
                      insert into cierre values(2020,02,1,'2020-02-01','2020-02-12','2020-02-14');
                      insert into cierre values(2020,02,2,'2020-02-02','2020-02-12','2020-02-15');
                      insert into cierre values(2020,02,3,'2020-02-03','2020-02-12','2020-02-16');
                      insert into cierre values(2020,02,4,'2020-02-04','2020-02-12','2020-02-17');
                      insert into cierre values(2020,02,5,'2020-02-05','2020-02-12','2020-02-18');
                      insert into cierre values(2020,02,6,'2020-02-06','2020-02-12','2020-02-19');
                      insert into cierre values(2020,02,7,'2020-02-07','2020-02-12','2020-02-20');
                      insert into cierre values(2020,02,8,'2020-02-08','2020-02-12','2020-02-21');
                      insert into cierre values(2020,02,9,'2020-02-09','2020-02-12','2020-02-22');
                      insert into cierre values(2020,02,0,'2020-02-10','2020-02-12','2020-02-23');
                      insert into cierre values(2020,03,1,'2020-03-01','2020-03-12','2020-03-14');
                      insert into cierre values(2020,03,2,'2020-03-02','2020-03-12','2020-03-15');
                      insert into cierre values(2020,03,3,'2020-03-03','2020-03-12','2020-03-16');
                      insert into cierre values(2020,03,4,'2020-03-04','2020-03-12','2020-03-17');
                      insert into cierre values(2020,03,5,'2020-03-05','2020-03-12','2020-03-18');
                      insert into cierre values(2020,03,6,'2020-03-06','2020-03-12','2020-03-19');
                      insert into cierre values(2020,03,7,'2020-03-07','2020-03-12','2020-03-20');
                      insert into cierre values(2020,03,8,'2020-03-08','2020-03-12','2020-03-21');
                      insert into cierre values(2020,03,9,'2020-03-09','2020-03-12','2020-03-22');
                      insert into cierre values(2020,03,0,'2020-03-10','2020-03-12','2020-03-23');
                      insert into cierre values(2020,04,1,'2020-04-01','2020-04-12','2020-04-14');
                      insert into cierre values(2020,04,2,'2020-04-02','2020-04-12','2020-04-15');
                      insert into cierre values(2020,04,3,'2020-04-03','2020-04-12','2020-04-16');
                      insert into cierre values(2020,04,4,'2020-04-04','2020-04-12','2020-04-17');
                      insert into cierre values(2020,04,5,'2020-04-05','2020-04-12','2020-04-18');
                      insert into cierre values(2020,04,6,'2020-04-06','2020-04-12','2020-04-19');
                      insert into cierre values(2020,04,7,'2020-04-07','2020-04-12','2020-04-20');
                      insert into cierre values(2020,04,8,'2020-04-08','2020-04-12','2020-04-21');
                      insert into cierre values(2020,04,9,'2020-04-09','2020-04-12','2020-04-22');
                      insert into cierre values(2020,04,0,'2020-04-10','2020-04-12','2020-04-23');
                      insert into cierre values(2020,05,1,'2020-05-01','2020-05-12','2020-05-14');
                      insert into cierre values(2020,05,2,'2020-05-02','2020-05-12','2020-05-15');
                      insert into cierre values(2020,05,3,'2020-05-03','2020-05-12','2020-05-16');
                      insert into cierre values(2020,05,4,'2020-05-04','2020-05-12','2020-05-17');
                      insert into cierre values(2020,05,5,'2020-05-05','2020-05-12','2020-05-18');
                      insert into cierre values(2020,05,6,'2020-05-06','2020-05-12','2020-05-19');
                      insert into cierre values(2020,05,7,'2020-05-07','2020-05-12','2020-05-20');
                      insert into cierre values(2020,05,8,'2020-05-08','2020-05-12','2020-05-21');
                      insert into cierre values(2020,05,9,'2020-05-09','2020-05-12','2020-05-22');
                      insert into cierre values(2020,05,0,'2020-05-10','2020-05-12','2020-05-23');
                      insert into cierre values(2020,06,1,'2020-06-01','2020-06-12','2020-06-14');
                      insert into cierre values(2020,06,2,'2020-06-02','2020-06-12','2020-06-15');
                      insert into cierre values(2020,06,3,'2020-06-03','2020-06-12','2020-06-16');
                      insert into cierre values(2020,06,4,'2020-06-04','2020-06-12','2020-06-17');
                      insert into cierre values(2020,06,5,'2020-06-05','2020-06-12','2020-06-18');
                      insert into cierre values(2020,06,6,'2020-06-06','2020-06-12','2020-06-19');
                      insert into cierre values(2020,06,7,'2020-06-07','2020-06-12','2020-06-20');
                      insert into cierre values(2020,06,8,'2020-06-08','2020-06-12','2020-06-21');
                      insert into cierre values(2020,06,9,'2020-06-09','2020-06-12','2020-06-22');
                      insert into cierre values(2020,06,0,'2020-06-10','2020-06-12','2020-06-23');
                      insert into cierre values(2020,07,1,'2020-07-01','2020-07-12','2020-07-14');
                      insert into cierre values(2020,07,2,'2020-07-02','2020-07-12','2020-07-15');
                      insert into cierre values(2020,07,3,'2020-07-03','2020-07-12','2020-07-16');
                      insert into cierre values(2020,07,4,'2020-07-04','2020-07-12','2020-07-17');
                      insert into cierre values(2020,07,5,'2020-07-05','2020-07-12','2020-07-18');
                      insert into cierre values(2020,07,6,'2020-07-06','2020-07-12','2020-07-19');
                      insert into cierre values(2020,07,7,'2020-07-07','2020-07-12','2020-07-20');
                      insert into cierre values(2020,07,8,'2020-07-08','2020-07-12','2020-07-21');
                      insert into cierre values(2020,07,9,'2020-07-09','2020-07-12','2020-07-22');
                      insert into cierre values(2020,07,0,'2020-07-10','2020-07-12','2020-07-23');
                      insert into cierre values(2020,08,1,'2020-08-01','2020-08-12','2020-08-14');
                      insert into cierre values(2020,08,2,'2020-08-02','2020-08-12','2020-08-15');
                      insert into cierre values(2020,08,3,'2020-08-03','2020-08-12','2020-08-16');
                      insert into cierre values(2020,08,4,'2020-08-04','2020-08-12','2020-08-17');
                      insert into cierre values(2020,08,5,'2020-08-05','2020-08-12','2020-08-18');
                      insert into cierre values(2020,08,6,'2020-08-06','2020-08-12','2020-08-19');
                      insert into cierre values(2020,08,7,'2020-08-07','2020-08-12','2020-08-20');
                      insert into cierre values(2020,08,8,'2020-08-08','2020-08-12','2020-08-21');
                      insert into cierre values(2020,08,9,'2020-08-09','2020-08-12','2020-08-22');
                      insert into cierre values(2020,08,0,'2020-08-10','2020-08-12','2020-08-23');
                      insert into cierre values(2020,09,1,'2020-09-01','2020-09-12','2020-09-14');
                      insert into cierre values(2020,09,2,'2020-09-02','2020-09-12','2020-09-15');
                      insert into cierre values(2020,09,3,'2020-09-03','2020-09-12','2020-09-16');
                      insert into cierre values(2020,09,4,'2020-09-04','2020-09-12','2020-09-17');
                      insert into cierre values(2020,09,5,'2020-09-05','2020-09-12','2020-09-18');
                      insert into cierre values(2020,09,6,'2020-09-06','2020-09-12','2020-09-19');
                      insert into cierre values(2020,09,7,'2020-09-07','2020-09-12','2020-09-20');
                      insert into cierre values(2020,09,8,'2020-09-08','2020-09-12','2020-09-21');
                      insert into cierre values(2020,09,9,'2020-09-09','2020-09-12','2020-09-22');
                      insert into cierre values(2020,09,0,'2020-09-10','2020-09-12','2020-09-23');
                      insert into cierre values(2020,10,1,'2020-10-01','2020-10-12','2020-10-14');
                      insert into cierre values(2020,10,2,'2020-10-02','2020-10-12','2020-10-15');
                      insert into cierre values(2020,10,3,'2020-10-03','2020-10-12','2020-10-16');
                      insert into cierre values(2020,10,4,'2020-10-04','2020-10-12','2020-10-17');
                      insert into cierre values(2020,10,5,'2020-10-05','2020-10-12','2020-10-18');
                      insert into cierre values(2020,10,6,'2020-10-06','2020-10-12','2020-10-19');
                      insert into cierre values(2020,10,7,'2020-10-07','2020-10-12','2020-10-20');
                      insert into cierre values(2020,10,8,'2020-10-08','2020-10-12','2020-10-21');
                      insert into cierre values(2020,10,9,'2020-10-09','2020-10-12','2020-10-22');
                      insert into cierre values(2020,10,0,'2020-10-10','2020-10-12','2020-10-23');
                      insert into cierre values(2020,11,1,'2020-11-01','2020-11-12','2020-11-14');
                      insert into cierre values(2020,11,2,'2020-11-02','2020-11-12','2020-11-15');
                      insert into cierre values(2020,11,3,'2020-11-03','2020-11-12','2020-11-16');
                      insert into cierre values(2020,11,4,'2020-11-04','2020-11-12','2020-11-17');
                      insert into cierre values(2020,11,5,'2020-11-05','2020-11-12','2020-11-18');
                      insert into cierre values(2020,11,6,'2020-11-06','2020-11-12','2020-11-19');
                      insert into cierre values(2020,11,7,'2020-11-07','2020-11-12','2020-11-20');
                      insert into cierre values(2020,11,8,'2020-11-08','2020-11-12','2020-11-21');
                      insert into cierre values(2020,11,9,'2020-11-09','2020-11-12','2020-11-22');
                      insert into cierre values(2020,11,0,'2020-11-10','2020-11-12','2020-11-23');
                      insert into cierre values(2020,12,1,'2020-12-01','2020-12-12','2020-12-14');
                      insert into cierre values(2020,12,2,'2020-12-02','2020-12-12','2020-12-15');
                      insert into cierre values(2020,12,3,'2020-12-03','2020-12-12','2020-12-16');
                      insert into cierre values(2020,12,4,'2020-12-04','2020-12-12','2020-12-17');
                      insert into cierre values(2020,12,5,'2020-12-05','2020-12-12','2020-12-18');
                      insert into cierre values(2020,12,6,'2020-12-06','2020-12-12','2020-12-19');
                      insert into cierre values(2020,12,7,'2020-12-07','2020-12-12','2020-12-20');
                      insert into cierre values(2020,12,8,'2020-12-08','2020-12-12','2020-12-21');
                      insert into cierre values(2020,12,9,'2020-12-09','2020-12-12','2020-12-22');
                      insert into cierre values(2020,12,0,'2020-12-10','2020-12-12','2020-12-23');`)
	if er != nil {
		log.Fatal(er)
	}
	fmt.Printf("Se cargaron los cierres\n")
}

func cargarTarjeta() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, er := db.Exec(`insert into tarjeta values('5800279369813969',  2, '201703', '201903', '8541', 100000.00, 'anulada');      
					insert into tarjeta values('6674862656844653',  3, '201703', '202506', '5576', 38000.00,  'vigente');
					insert into tarjeta values('3210781489519196',  4, '201704', '202811', '3357', 40000.00,  'vigente');
					insert into tarjeta values('0025215961815467',  5, '201705', '203009', '9317', 10000.00,  'vigente');
					insert into tarjeta values('5201749684282950',  6, '201601', '202407', '7031', 11000.00,  'vigente');
					insert into tarjeta values('9350583764311946',  7, '201802', '202405', '1397', 85000.00,  'vigente');
					insert into tarjeta values('0202829361569213',  8, '201903', '202709', '5088', 25000.00,  'vigente');
					insert into tarjeta values('6374012766885347',  9, '202001', '202108', '9901', 10000.00,  'vigente');
					insert into tarjeta values('9585760797853770', 10, '202010', '203001', '4788', 30000.00, 'vigente');
					insert into tarjeta values('3674656314167645', 11, '201709', '202503', '6321', 70000.00, 'vigente');
					insert into tarjeta values('2040329005482057',  1, '201709', '202104', '4579', 80000.00,  'suspendida');
					insert into tarjeta values('9874037341519263', 12, '202007', '202512', '6584', 82000.00, 'vigente');
					insert into tarjeta values('6547338853663039', 13, '201905', '202412', '5104', 400000.00,'vigente');
					insert into tarjeta values('3566111384747251', 14, '201711', '202605', '4857', 60000.00, 'vigente');
					insert into tarjeta values('9741112487023475', 15, '201802', '202401', '8137', 180000.00,'vigente');
					insert into tarjeta values('5911718181747681', 16, '201901', '202302', '9632', 230000.00,'vigente');
					insert into tarjeta values('1174736395002184', 17, '201906', '202212', '7548', 81500.00, 'vigente');
					insert into tarjeta values('9585915783909434', 18, '201907', '202309', '3574', 74500.00,'vigente');
					insert into tarjeta values('6688134012375322',  0, '201803', '202204', '3573', 97980.00, 'suspendida');
					insert into tarjeta values('1122007172229162', 19, '201801', '202301', '9519', 85500.00,'vigente');
					insert into tarjeta values('6574547618543472',  1, '201809', '202204', '8376', 39800.00, 'vigente');
					insert into tarjeta values('1010833689558208',  0, '201811', '202012', '1025', 62800.00, 'vigente');`)
	if er != nil {
		log.Fatal(er)
	}
	fmt.Printf("Tarjetas cargadas")
}

//------------------------------ METODOS PARA GENERAR CONSUMOS, RESUMEN, ALERTAS Y RECHAZOS ----------------------------------------------------------------

func crearStoredProcedure() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable") // conexion a psql
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, bug := db.Exec(`create or replace function autorizar(nrotarj char(16),cod_seg char(4),nrocomercio int, monto decimal(7,2)) returns boolean as $$
					declare
						suma decimal (7,2):=0;
						fila record;
						m compra.monto%type;
						copia char(8):='';
						indice int:=(select max(nrorechazo) from rechazo)+1;
					begin
						if indice is null then
							indice:=0;
						end if;
						select * into fila from tarjeta where nrotarjeta=nrotarj; --busca la tarjeta en donde nrotarjeta sea igual al pasado por parametros
						if not found then -- if no la encontró al nrotarjeta se genera un rechazo
								insert into rechazo values(indice,nrotarj,nrocomercio,current_timestamp,monto,'Tarjeta no válida ó no vigente');
								return false;
						end if;

						select * into fila from tarjeta where nrotarjeta=nrotarj and codseguridad=cod_seg; --busca todos los datos de la tarjeta en donde el cod_seguridad sea igual pasado por parametro
						if not found then -- si no lo encuentra el cod_seguridad se genera un rechazo
							insert into rechazo values(indice,nrotarj,nrocomercio,current_timestamp,monto,'Código de seguridad inválido');
							return false;
						end if;

						for m  in select c.monto from compra c where nrotarj=c.nrotarjeta and c.pagado=false loop -- recorro los montos y los acumulo
							if m is not null then
								suma:=suma+m;
							end if;
						end loop;
						suma:=suma+monto; -- sumo el monto actual
						select * into fila from tarjeta where nrotarjeta=nrotarj; -- busco la fila que coincida la tarjeta
						copia :=fila.validahasta;
						copia:=copia||'01'; -- concateno el dia
						if to_date(copia, 'YYYYMMDD') < to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') then -- verifico si la fecha de la tarjeta se vencio
							insert into rechazo values(indice,nrotarj,nrocomercio,current_timestamp,monto,'Plazo de vigencia expirado');
							return false;
						elsif fila.estado='suspendida' then --verifica si la tarjeta esta suspendida
							insert into rechazo values(indice,nrotarj,nrocomercio,current_timestamp,monto,'La tárjeta se encuentra suspendida');
							return false;
						elsif fila.limitecompra < suma then-- verifico si la suma de las compras supera al limite de la tarjeta
							insert into rechazo values(indice,nrotarj,nrocomercio,current_timestamp,monto,'Supera límite de tárjeta');
							return false;
						end if;
						return true;
					end;
					$$ language plpgsql;

					
					
					create or replace function resumen(_nrocliente int,_mes int,_año int) returns void as $$
					declare
						pkdetalle int;
						pkcabecera int;
						total decimal(8,2)=0;
						nrotarj tarjeta.nrotarjeta%type;
						fila record;
						--fila2 record;
						fila3 record; 
						nombrecomercio comercio.nombre%type;
					begin
						for nrotarj in select nrotarjeta from tarjeta where nrocliente=_nrocliente loop
							pkcabecera:=(select max(nroresumen+1)from cabecera); 
							if pkcabecera is null then
								pkcabecera:=0;
							end if;
							for fila in select nrocomercio,fecha,monto from compra where nrotarj=nrotarjeta and pagado=false loop
								for nombrecomercio in select nombre from comercio where nrocomercio=fila.nrocomercio loop
									pkdetalle:=(select max(nrolinea+1) from detalle);
									if pkdetalle is null  then
										pkdetalle:=0;
									end if;
									insert into detalle values(pkcabecera,pkdetalle,fila.fecha,nombrecomercio,fila.monto);
									total:=total+fila.monto;
								end loop;
							end loop;
							for fila in select * from cliente c where _nrocliente=c.nrocliente loop
									for fila3 in select c.fechainicio,c.fechacierre,c.fechavto from cierre c where  c.anio=_año and c.mes=_mes and c.terminacion=to_number(substr(nrotarj,char_length(nrotarj),1),'9') loop
											insert into cabecera values(pkcabecera,fila.nombre,fila.apellido,fila.domicilio,nrotarj,fila3.fechainicio,fila3.fechacierre,fila3.fechavto,total);
									end loop;
							end loop;
						end loop;
						   
								
					end;
					$$ language plpgsql;

					
					
					
					create or replace function alerta_compra() returns trigger as $$
					declare 
						fila record;
						fila2 record;
						fila3 record;
						secondsfila int:=0;
						secondsnew  int:=0;
						pk int;
					begin
						 if(select count(nrotarjeta)from compra where nrotarjeta=new.nrotarjeta and -- verifica que se haya hecho 2 o mas compras en el mismo dia
							to_char(fecha,'YYYYMMDD')=to_char(new.fecha,'YYYYMMDD') )>=2 then
							select * into fila from compra where nrooperacion=(select max(nrooperacion-1)from compra) and nrotarjeta=new.nrotarjeta;  -- busca la compra con el numero de operacion mas alto y el nrotarjeta correspondiente
							secondsfila:=secondsfila + (to_number(to_char(fila.fecha,'HH'),'99')*60*60);
							secondsfila:=secondsfila + (to_number(to_char(fila.fecha,'MI'),'99')*60);
							secondsfila:=secondsfila + to_number(to_char(fila.fecha,'MI'),'99');
							secondsnew:=secondsnew + (to_number(to_char(new.fecha,'HH'),'99')*60*60);
							secondsnew:=secondsnew + (to_number(to_char(new.fecha,'MI'),'99')*60);
							secondsnew:=secondsnew + to_number(to_char(new.fecha,'MI'),'99');
							secondsnew:=secondsnew - secondsfila; --calcula la diferencia de tiempo de las compras
							select * into fila2 from comercio where fila.nrocomercio=nrocomercio; -- busca los comercios donde se hicieron las compras
							select * into fila3 from comercio where new.nrocomercio=nrocomercio;
							if secondsnew<60 then
								if fila2.codigopostal=fila3.codigopostal and fila2.domicilio!=fila3.domicilio then
									pk :=(select max(nrooperacion+1) from compra)+1;
									if pk is null then
										pk:=0;
									end if;
									insert into rechazo values(pk,new.nrotarjeta,new.nrocomercio,new.fecha,new.monto,'compra en 1min');
									return new;
								end if;
							elsif secondsnew>60 and secondsnew<300 then
								if fila2.domicilio!=fila3.domicilio and fila2.codigopostal!=fila3.codigopostal then
									pk :=(select max(nrooperacion+1) from compra);
									if pk is null then
										pk:=0;
									end if;
									insert into rechazo values(pk,new.nrotarjeta,new.nrocomercio,new.fecha,new.monto,'compra en 5min');
									return new;
								end if;  
							end if;
						 end if;
						 return null;

					end;
					$$ language plpgsql;



					
					
					

					create trigger alerta_compra_trg
					after insert on compra
					for each row
					execute procedure alerta_compra();


					
					
					
					create or replace function insertar_alerta() returns trigger as $$
					declare
						pkalerta alerta.nroalerta%type:=(select max(nroalerta) from alerta)+1;
						fila record;
					begin
						if pkalerta is null then
							pkalerta:=0;
						end if; 
						if new.motivo='Tarjeta no válida ó no vigente' then
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,0,'Se generó un rechazo debido a que la tarjeta no es válida');
							return new;
						elsif new.motivo='Código de seguridad inválido' then  
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,0,'Se generó un rechazo porque el codigo se seguridad no es válido');
							return new;
						elsif new.motivo='Plazo de vigencia expirado' then
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,0,'Se generó un rehazo debido a que la tarjeta está vencida');
							return new;
						elsif new.motivo='La tárjeta se encuentra suspendida' then
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,0,'Se generó un rechazo porque la tarjeta esta suspendida');
							return new;
						elsif new.motivo='compra en 1min' then
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,1,'Se generó 2 compras en menos de 1min en comercios distintos pero un una misma localidad');
							return new;
						elsif new.motivo='compra en 5min' then
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,5,'Se generó 2 compras en menos de 5min en comercios de distinta localidad');
							return new;
						end if;
						if (select count(nrotarjeta) from rechazo where to_char(fecha,'YYYYMMDD')=to_char(new.fecha,'YYYYMMDD') and motivo='Supera límite de tárjeta')>=2 then
								update tarjeta set estado='suspendida' where nrotarjeta=new.nrotarjeta;
								insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,32,'Se suspendió la tarjeta debido a la cantidad de veces en cual la tarjeta fue rechazada por exceso de limíte ');
								return new;
						else
							insert into alerta values(pkalerta,new.nrotarjeta,new.fecha,new.nrorechazo,0,'Se generó un rechazo debido a que la tarjeta superó los limites de compra');
							return new;
						end if; 
						return null; 
					end;
					$$ language plpgsql;

					
					

					create trigger insertar_alerta_trg
					after insert on rechazo 
					for each row
					execute procedure insertar_alerta();

					
					
					
					create or replace function autorizar_compra() returns void as $$
					declare
						fila record;
						indice int;
					begin 
						for fila in select * from consumo loop
							if(select autorizar(fila.nrotarjeta,fila.codseguridad,fila.nrocomercio,fila.monto)) then
								indice:=(select max(nrooperacion) from compra)+1;
								if indice is null then
									indice:=0;
								end if;    
								insert into compra values(indice,fila.nrotarjeta,fila.nrocomercio,current_timestamp,fila.monto,false);
							end if;
						end loop;
					end;
					$$ language plpgsql;


					
					
					create or replace function generar_resumen() returns void as $$
					declare 
						año int:=2020;
						mes int:=12;
					begin
						for i in 0..19 loop
							perform resumen(i,mes,año);
						end loop;
					end;
					$$ language plpgsql;`)
	if bug != nil {
		log.Fatal(err)
	}
	fmt.Printf(">>>Stored Procedure y triggers cargados!\n")

}

func generarResumen() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable") // conexion a postgres
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, erro := db.Exec(`select generar_resumen();`) // ejecuta comando para ejecutar generar_resumen()
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Printf("Se generó los resumenes de las compras\n")
}

func autorizar_compra() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=bddtp sslmode=disable") // conexion a postgres
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, erro := db.Exec(`select autorizar_compra();`) // ejecuta comando para ejecutar autorizar_compra()
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Printf("Se generó las compras\n")

}

